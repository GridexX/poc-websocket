# POC Websocket

This is a proof of concept for a websocket server in Golang with echo.

##


## Usage

```bash
go run gorilla/main.go
```

Open your web browser and go to http://localhost:1323

You should see a message in the console and `Hello Client` appearing in the browser.

The request is multi-threaded, so you can open multiple tabs and see the message appearing in all of them.

Also you can request other API route like `localhost:1323/hello` and see the message appearing in the browser.